Source: poedit
Section: text
Priority: optional
Maintainer: Debian l10n developers <debian-l10n-devel@lists.alioth.debian.org>
Uploaders: Andreas Rönnquist <gusnan@debian.org>,
           Gianfranco Costamagna <locutusofborg@debian.org>
Build-Depends: asciidoc-base,
               debhelper-compat (= 13),
               gettext,
               libboost-dev,
               libboost-iostreams-dev,
               libboost-regex-dev,
               libboost-system-dev,
               libboost-thread-dev,
               libcld2-dev [!s390x !hppa !powerpc !ppc64 !sparc64],
               libcpprest-dev,
               libdb++-dev,
               libgtk-3-dev,
               libgtkspell3-3-dev,
               libicu-dev,
               liblucene++-dev,
               libpugixml-dev,
               libsecret-1-dev,
               libwxgtk3.0-gtk3-dev,
               libwxgtk-webview3.0-gtk3-dev,
               nlohmann-json3-dev,
               xsltproc,
               xmlto,
               zip
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://www.poedit.net/
Vcs-Git: https://salsa.debian.org/l10n-team/poedit.git
Vcs-Browser: https://salsa.debian.org/l10n-team/poedit

Package: poedit
Architecture: any
Depends: gettext,
         poedit-common (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: gettext catalog editor
 Poedit is an editor for gettext catalogs (.po files). It aims to provide a
 convenient approach to editing catalogs. It features UTF-8 support, fuzzy and
 untranslated records highlighting, whitespace highlighting, references browser,
 header editing and can be used to create new catalogs or update existing
 catalogs from source code with a single click. It is built with wxWidgets
 toolkit.

Package: poedit-common
Architecture: all
Recommends: poedit
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Common files for poedit
 Poedit is an editor for gettext catalogs (.po files). It aims to provide a
 convenient approach to editing catalogs. It features UTF-8 support, fuzzy and
 untranslated records highlighting, whitespace highlighting, references browser,
 header editing and can be used to create new catalogs or update existing
 catalogs from source code with a single click. It is built with wxWidgets
 toolkit.
 .
 This package includes documentation, examples and locale files for the
 Debian poedit packages that are common for all architectures.
